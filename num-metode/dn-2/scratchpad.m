% DENSITY FIT

% Known datapoints y_i(x_i).
x_data = [0,2000,4000,6000,8000,10000,15000,20000,25000,30000,40000]'
y_data = [1.225,1.007,0.8194,0.6601,0.5258,0.4135,0.1948,0.08891,0.04008,0.01841,0.003996]'
% The model function is f(x_j) = ∑_i β_i φ_i(x_j). We're looking for a
% parameter vector β that minimizes the sum of squared residuals
%   r_i = y_i - f(beta; x_i).
% Such beta can be shown to satisfy
%   M'M β = My.  (1)
% where M is defined as
%   M_ij := φ_i(x_j).
phi_1 = @(x) 1
phi_2 = @(x) ((x - 4e4)/4e4)^2
phi_3 = @(x) ((x - 4e4)/4e4)^4
stack = {
    arrayfun(phi_1,x_data),
    arrayfun(phi_2,x_data),
    arrayfun(phi_3,x_data)
}
M = cat(2, stack{:})
[n,N] = size(M)
% If we QR decompose M, system (1) reduces to
%   Rβ = Q'y =: c.
[c,R] = qr(sparse(M),y_data)
% Backwards substitute.
R = full(R)
beta = zeros(3,1)
for i = N:-1:1
  beta(i) = (c(i) - R(i,i+1:N)*beta(i+1:N)) / R(i,i)
end
% Plot.
scatter(x_data, x_data)
f = @(x) beta(1)*phi_1(x) + beta(2)*phi_2(x) + beta(3)*phi_3(x)
x_lin = linspace(-1e4,5e4,501)
plot(x_lin, arrayfun(f,x_lin))
% Find zero for fit.
x_0 = fzero(@(x) (f(x) - 0.5), 4e4)

% DIFF EQUATION

% Motion of a skydiver is modeled by a differential equation
%   y'' = -g + ρSc/2m (y')^2.  (2)
% We have to bear in mind that g, ρ and Sc are not constants, but functions
%   g = g(y), ρ = ρ(y), Sc = Sc(t).
m = 128
g_0 = 9.81
r = 6371e3
g = @(y) (g_0 / (1+y/r)^2)
% Previously fitted density ρ.
rho = f
% Parachute function.
Sc_0 = 1.2 * 1.3
Sc_1 = (1.2+12) * 4*1.3
t_par = @(t) (t > 300)
Sc = @(t) (1 - t_par(t))*Sc_0 + t_par(t)*Sc_1
% Equation (2) can be rewriten as system of 1st order ODEs as
%   z' = G(t, z) := [v,-g + ρSc/2m v^2],
%   z = [y,v].
G = @(t,z) ([z(2), -g(z(1)) + rho(z(1))*Sc(t)/(2*m) * z(2)^2]')
% Initial [y_0,v_0].
z_0 = [4e4,0]'
% 1. First 260 s. Parachute is closed.
t_lin = linspace(0,250,1e3)
[t,z] = ode23s(G,t_lin,z_0)
s_1 = z_0(1) - z(end,1)
% 2. First 400 s. Parachite opens at t = 300s.
t_lin = linspace(0,400,1e3)
[t,z] = ode23s(G,t_lin,z_0)
rho_2 = rho(z(end,1))
% 3. First 50 s. With a new mass of M = m + 100 kg and an additional downwards
% force of 200 N.
M = m + 100
F_0 = 200
G = @(t,z) ([z(2), -g(z(1)) - F_0/M + rho(z(1))*Sc(t)/(2*M) * z(2)^2]')
t_lin = linspace(0,50,1e3)
[t,z] = ode23s(G,t_lin,z_0)
v_3 = abs(z(end,2))
