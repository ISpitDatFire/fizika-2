% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

\title{Meritve magnetnega polja z indukcijo}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Magnetno polje merimo z majhno tuljavico z veliko ovoji, postavljeno z osjo vzporedno
zunanjemu magnetnemu polju. Če je tuljava sestavljena iz $N$ zank s površino $S$, postavljenih pod kotom $\alpha$ glede na polje $\mathbf B$, se na tuljavi inducira napetost

\begin{equation*}
    U = -NS \cos\alpha \frac{\diff B}{\diff t}.
\end{equation*}

Ker je $N$ velik, se zunanji in notranji premer tuljave (v obliki diska) razlikujeta dovolj, da moramo to upoštevati. Če smatramo, da je število navojev na enoto ploščino povsod v tuljavi enako, dobimo

\begin{equation*}
   \diff U = -\pi r^2 \cos\alpha \frac{2Nr}{r_2^2 - r_1^2} \frac{\diff B}{\diff t} \diff r,
\end{equation*}

in po integraciji od $r_1$ do $r_2$

\begin{equation}
    U = -N \pi \left( \frac{r_1^2 + r_2^2}{2} \right)\cos\alpha  \frac{\diff B}{\diff t}.
    \label{eq:induced}
\end{equation}

Ker lahko merimo inducirano napetost, zanima nas pa magnetno polje $B$, izhod merilne tuljave priključimo na integrator -- posebno elektronsko vezje, ki na izhodu ustvari napetost, sorazmerno s časovnim integralom vhodne napetosti

\begin{equation*}
    \widetilde U = -\frac{1}{RC} \int_{t_0}^t U \,\diff t.
\end{equation*}

Če uporabimo izraz~(\ref{eq:induced}), je to naprej enako

\begin{equation}
    \widetilde U = N \pi \left( \frac{r_1^2 + r_2^2}{2RC} \right) \cos\alpha (B - B_0).
    \label{eq:int}
\end{equation}

\section{Naloga}

\begin{enumerate}
    \item Izmeri odvisnost gostote magnetnega polja $B$ na osi tokovne zanke z oddaljenostjo od njenega središča.
    \item Izmeri relacijo med jakostjo električnega toka $I$ in gostoto magnetnega polja $B$ v elektromagnetu.
\end{enumerate}

\section{Meritve in račun}

Poznamo dimenzije prve merilne tuljavice, število ovojev, njen notranji in zunanji premer

\begin{align*}
    N_{\mathrm a} &= 2000, \\
    2r_1 &= (18.00 \pm 0.1)\,\si{mm}, \\
    2r_2 &= (23.00 \pm 0.5)\,\si{mm}. \\
\end{align*}

Poznamo tudi lastnosti integratorja, upornost in kapacitivnost

\begin{align*}
    R &= (10 \pm 0.5)\,\si{k\ohm}, \\
    C &= (1.0 \pm 0.1)\,\si{\mu F}.
\end{align*}

In poznamo dimenzije velike tuljave, število ovojev, polmer in njeno debelino

\begin{align*}
    N_{\mathrm A} &= 200, \\
    r_{\mathrm A} &= (250 \pm 2)\,\si{mm}, \\
    d_{\mathrm A} &= (2.4 \pm 0.05)\,\si{cm}.
\end{align*}

Vklopimo napajalnik, da po tuljavi $\mathrm A$ teče tok

\begin{equation*}
    I_A = (4.40 \pm 0.05)\,\si{A}.
\end{equation*}

Prižgemo integrator in merilno tuljavico umaknemo daleč od tuljave. Tako je v enačbi~\ref{eq:int}  začetni $B_0 = 0$. Tuljavico nato prinesemo na os velike tuljave, na višino $h'$ nad zgornjo plastično ploščo. Dejanska razdalja od merilne tuljavice do točke merjenja je torej $h = h' + d_{\mathrm A}/2$.

Iz izmerjenih integriranih $\widetilde U$ lahko po enačbi~\ref{eq:int} izračunamo $B$ na dani višini. Za to pričakujemo, da bo enaka

\begin{equation*}
    B = \frac{\mu_0 N_A I_A}{2(r_A^2 + h^2)^{\frac{3}{2}}},
\end{equation*}

pri čemer je $\mu_0$ permiablinost vakuma. Meritve in napoved nato primerjamo (slika~\ref{fig:big-coil}).

\begin{figure}
    \centering
    \includegraphics{big-coil.pdf}
    \caption{Pričakovana ter izmerjena gostota magnetnega polja (z napakami) pri različnih oddaljenostih $h$ od središča velike tuljave.}
    \label{fig:big-coil}
\end{figure}

Zdaj merilno tuljavico zamenjamo za tuljavico z manj ovoji, pri čemer notranji in zunanji polmer $r_1, r_2$ ostaneta nespremenjena

\begin{align*}
    N_{\mathrm b} &= 200, \\
    2r_1 &= (18.00 \pm 0.1)\,\si{mm}, \\
    2r_2 &= (23.00 \pm 0.5)\,\si{mm}. \\
\end{align*}

Merilno tuljavico postavimo v režo močnega elektromagneta in ponovno izmerimo integrirane $\widetilde U$. Enako kot prej iz integriranega $\widetilde U$ po enačbi~\ref{eq:int} izračunamo $B$, tokrat pri različnih tokovih, s katerimi napajamo elektromagnet. Iz dobljenih točk (slika~\ref{fig:slit}) se jasno vidi linearna odvisnost $B(I) = kI$. Če skozi prilagodimo premico, dobimo koeficient

\begin{equation*}
    k = (0.138 \pm 0.08)\,\si{T/A}.
\end{equation*}

Če bi torej elektromagnet pri enakem toku hoteli nadomestiti z dolgo prazno tuljavo, v kateri je gostota

\begin{equation*}
    B = \frac{\mu_0 N}{L} I,
\end{equation*}

bi potrebovali navitje gostote

\begin{equation*}
    \frac{N}{L} = (110000 \pm 6000)\,\si{m^{-1}}.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{slit.pdf}
    \caption{Merive magnetne gostote v reži elektromagneta pri različnih tokovih skozi elektromagnet. Pikčasto je prikazana prilegajoča premica z naklonom $k$.}
    \label{fig:slit}
\end{figure}

\end{document}
