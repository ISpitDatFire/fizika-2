% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\setlength{\skip\footins}{1.5cm}

\title{Zemljino magnetno polje}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Zemljo obdaja šibko magnetno polje. V tej vaji ga bomo izmerili z dvema različnima metodama. Zanimajo nas tudi razlike med metodama, prednosti in slabosti vsake.

\subsection{Kompenzacijska metoda}

Pri tej metodi uporabimo kompas in tuljavo, katere dolgo os postavimo na mizo pod majhnim kotom $\delta$ (slika~\ref{fig:komp}) Nato tuljavo priključimo na napajanje in spreminjamo tok ter gledamo obnašanje igle. Če skozi tuljavo ne teče tok, bo igla kazala v smer Zemljinega polja. Če pa je polje tuljave močnejše od Zemljinega, se bo igla poravnala z njim, pod kotom $\delta$ glede na Zemljino polje. Nekje vmes bosta torej polji enaki in igla bo ravno simetrala kota med $\mathbf B_t$ in $\mathbf B_Z$ (slika~\ref{fig:komp}). Če pri tem poznamo polje tuljave, posredno poznamo tudi Zemljino polje. Polje tuljave lahko preprosto izračunamo, a ker je tuljava prekratka, da bi jo opisali kot dolgo tuljavo, uporabimo natančnejšo formulo

\begin{equation}
    B_t = \frac{\nu I}{\sqrt{L^2 + d^2}},
    \label{eq:B_t}
\end{equation}

pri čemer je $N$ število ovojev, $I$ tok, ki teče skozi tuljavo, $L$ dolžina ter $d$ premer tuljave.

\begin{figure}
    \centering
    \import{}{compensation.pdf_tex}
    \caption{Skica merjenja po kompenzacijski metodi. $\mathbf B_t$ je polje tuljave, $\mathbf B_Z$ pa je Zemljino magnetno polje. Velikosti polj sta ravno enaki, tako da je označen kot enak $\frac{\delta}{2}$.}
    \label{fig:komp}
\end{figure}

\subsection{Gaussova metoda}

Pri Gaussovi metodi izmerimo dve količini, prvo izmerimo produkt, nato pa še razmerje magnetnega dipola $p$ ter Zemljinega magnetnega polja $B_Z$.

Za meritev produkta $p B_Z$ uporabimo viseče vodoravno nihalo, kjer se dipol (paličast magnet) v vodoravni ravnini vrti okoli osi $z$. Za majhne začetne odmike $\phi_0$ velja

\begin{equation*}
    J \ddot\varphi + \frac{pB_Z}{J} \varphi = 0,
\end{equation*}

torej je nihanje harmonično in je njegova perioda

\begin{equation}
    t_0 = 2\pi \frac{J}{p B_Z}.
    \label{eq:period}
\end{equation}

V drugem delu eksperimenta pomerimo razmerje med konstanim Zemljinim ter z razdaljo padajočim poljem dipola. Na leseno letev s kompasom, ki je orientirana v smeri Zemljinega polja, postavimo na razdaljo $r$ od kompasa dipol. Vemo, da je polje dipola na $\mathbf r$, krajevnem vektorju od dipola do kompasa, enako

\begin{equation*}
    \mathbf B_p = \frac{\nu}{4\pi} \left( -\frac{\mathbf p}{r^3} + \frac{3(\mathbf p \cdot \mathbf r)}{r^4} \frac{\mathbf r}{r} \right).
\end{equation*}

Ker magnet postavimo z dolgo osjo pravokotno na letev, je skalarni produkt $\mathbf p \cdot \mathbf r = 0$. Polje pri kompasu kaže vzporedno z dipolom in njegova velikost je preprosto $\frac{\mu_0}{4\pi} p$. Torej vsota $\mathbf B_p + \mathbf B_Z$ z letvijo oklepa nek kot $\alpha$, katerega $\tan\alpha$ je ravno $\frac{B_p}{B_Z}$. Velja torej

\begin{equation}
    \tan\alpha = \frac{\mu_0}{4\pi r^3} \frac{p}{B_Z},
    \label{eq:ratio}
\end{equation}

iz česar lahko po eksperimentu izračunamo razmerje $\frac{p}{B_Z}$.

\section{Naloga}

\begin{enumerate}
    \item Izmeri vodoravno komponento gostote zemeljskega magnetnega polja s kompenzacijo in po Gaussovi metodi.
    \item Določi magnetni moment paličastega magneta.
\end{enumerate}

\section{Meritve in račun}

\subsection{Kompenzacijska metoda}

Izmerimo število ovojev, dolžino in premer tuljave

\begin{align*}
    N &= (59.8 \pm 0.5)\,\si{cm}, \\
    L &= (60 \pm 1)\,\si{cm}, \\
    d &= (12.7 \pm 0.6)\,\si{cm}.
\end{align*}

Vredno je omeniti, da je premer tuljave na eni strani kar centimeter večji kot na drugi. Pri tokovim v eno, nato pa še v drugo smer izmerimo, da je igla uravnovešena, torej pri kotu $\frac{\delta}{2}$ za tok

\begin{equation*}
    I = (0.17 \pm 0.03)\,\si{A}.
\end{equation*}

Napako dovimo iz specifikacije multimetra (model Finest 703). Iz enačbe~(\ref{eq:B_t}) lahko nato izračunamo, da je polje tuljave, torej tudi Zemljino polje enako

\begin{equation*}
    B_Z = (21.2 \pm 0.6)\,\si{\mu T}
\end{equation*}

\subsection{Gaussova metoda}

Magnet stehtamo in pomerimo premer in njegovo dolžino

\begin{align*}
    m_m &= (44.0 \pm 0.5)\,\si{mm}, \\
    2r &= (16.0 \pm 0.5)\,\si{mm}, \\
    l &= (45.0 \pm 0.5)\,\si{mm}.
\end{align*}

Prav tako stehtamo pstični tulec in pomerimo notranji, zunanji premer ter njegovo dolžino

\begin{align*}
    m_t &= (6.0 \pm 0.5)\,\si{mm}, \\
    2r_1 &= (17.0 \pm 0.5)\,\si{mm}, \\
    2r_2 &= (19.0 \pm 0.5)\,\si{mm}, \\
    L &= (50.0 \pm 0.5)\,\si{mm}
\end{align*}

Iz teh dimenzij lahko po znanih formulah izračunamo vztrajnosti moment magneta in tulca

\begin{align*}
    J_m &= (8.1 \pm 0.1)\,\si{mgm^2}, \\
    J_t &= (1.5 \pm 0.1)\,\si{mgm^2}, \\
\end{align*}

nato pa obeh skupaj

\begin{equation*}
    J = J_m + J_p = (9.6 \pm 0.2)\,\si{mgm^2}.
\end{equation*}

\begin{table}
    \centering
    \begin{tabular}{c}
        $20t_0\,[\si{s}]$ \\
        \hline
        42.06 \\
        43.35 \\
        43.10
    \end{tabular}
    \caption{Meritve odmikov $x$ na merilu in kotov kompasove igle, ko je magnet na razdalji $r = |x - x_0|$.}
    \label{tab:periods}
\end{table}

Ko nato izmerimo po 20 nihanjih časov (tabela~\ref{tab:periods}), izvemo, da je nihajni čas

\begin{equation*}
    t_0 = (2.16 \pm 0.02)\,\si{s}.
\end{equation*}

Iz tega izračunamo, da je produkt

\begin{equation*}
    p B_Z = (84 \pm 2)\,\si{mJ}.
\end{equation*}

Nato magnet postavimo na letev s centimeterskimi oznakami, kjer je kompas postavljen na

\begin{equation*}
    x_0 = (78.0 \pm 0.1)\,\si{cm}.
\end{equation*}

\begin{table}
    \centering
    \begin{tabular}{r r|r}
        $x\,[\si{cm}]$ & $r\,[\si{cm}]$ & $\alpha\,[\si{\degree}]$ \\
        \hline
        30  & 48 & 8  \\
        35  & 43 & 10 \\
        36  & 42 & 10 \\
        38  & 40 & 11 \\
        40  & 38 & 14 \\
        41  & 37 & 15 \\
        43  & 35 & 18 \\
        45  & 33 & 19 \\
        48  & 30 & 24 \\
        50  & 28 & 28 \\
        53  & 25 & 36 \\
        105 & 27 & 14 \\
        110 & 32 & 9  \\
        115 & 37 & 3
    \end{tabular}
    \caption{Meritve odmikov $x$ na merilu in kotov kompasove igle, ko je magnet na razdalji $r = |x - x_0|$.}
    \label{tab:dipole}
\end{table}

Nato magnet premikamo po letvi in zapisujemo odmike $x$ ter kote na kompasu $\alpha$ (tabela~\ref{tab:dipole}). Pri tem moramo gledati, da je razdalja med magnetom in kompasom $r = |x - x_0|$ dovolj velika, da še vedno velja idealizacija točkastega dipola. Iz dobljenih meritev lahko potem po formuli~(\ref{eq:ratio}) izračunamo povprečno vrednost razmerja

\begin{equation*}
    \frac{p}{B_Z} = (110 \pm 40)\,\si{mJ/T^2}.
\end{equation*}

Iz produkta in razmerja lahko nato izračunamo Zemljino magnetno polje

\begin{equation*}
    B_Z = (27 \pm 5)\,\si{\mu T}.
\end{equation*}

\subsection{Primerjava}

\begin{table}
    \centering
    \begin{tabular}{l|r}
        metoda & $B_Z\,[\si{\mu T}]$ \\
        \hline
        kompenzacijska & $21.2 \pm 0.6$ \\
        Gaussova       & $27 \pm 5$
    \end{tabular}
    \caption{Primerjava rezultatov ene in druge metode.}
    \label{tab:primerjava}
\end{table}

Vidimo, da se rezultata približno ujemata, pri čemer je kompenzacijska metoda tista, ki nam bolj točno določi vrednost. Glavna prednost Gaussove metode je gotovo v tem, da zanjo ne potrebujemo inštrumenta za natančno merjenje toka.

\end{document}
