% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

\title{Piezoelektričnost}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Piezoelektrik je material, ki se na deformacijo odzove s polarizacijo. Velja tudi obratno -- v zunanjem električnem polju se deformira. Tipični predstavniki so feroelektrični kristali kot npr. 

V splošnem deformacijo podamo z napetostnim tenzorjem

\begin{equation*}
    T_{ij} = \frac{1}{2} \left( \frac{\diff F_i}{\diff S_j} + \frac{\diff F_j}{\diff S_i} \right).
\end{equation*}

Poleg deformacije je polarizacija odvisna od lastnosti materiala, od t. i. piezoelektričnega modula $d$, ki ga z napetostjo pomnožimo kot

\begin{equation*}
    P_i = d_{ijk} T_{jk}.
\end{equation*}

V našem primeru je piezoelektrični modul tak, da se na silo $F$ na ploskev $S$ odzove s polarizacijo le v eni smeri. Velikost polarizacije je torej preprosto

\begin{equation*}
    P = \frac{Fd}{S},
\end{equation*}

kjer je $d$ relevantna komponenta piezoelektričnega modula.

Po Gaussovem zakonu je naboj na ploskvici kristala $q = DS$. Ker je električna gostota preprosto $D = \varepsilon_0 E + P$ in je višina kristala $b$, je naboj na ploskvici

\begin{equation*}
    q = \frac{\varepsilon\varepsilon_0 S}{b}U + Fd = CU + Fd,
\end{equation*}

pri čemer je $C$ kacitivnost kondenzatorja z dimenzijami kristala. Ta naboj odteka in se porablja na uporu $R$ znotraj napetostnega sledilnika. Torej lahko zapišemo

\begin{align}
    U = RI = -R\dot{q} &= -RC\dot{U} - Rd\dot{F}, \notag\\
    U &= -\tau\dot{U} - Rd\dot{F},
    \label{eq:diff}
\end{align}

kjer je $\tau = RC$ časovna konstanta. Piezoelektrik ob času $t = 0$ obremenimo s silo $F_0$. To povzroči nek začeten skok napetosti $U_0$. To se zgodi na kratkem časovnem $-\Delta t$ do $0$, pri čemer je začetna napetost $U(-\Delta t) = 0$ in začetna sila prav tako $F(-\Delta t) = 0$. Z integracijo enačbe~\ref{eq:diff} torej dobimo

\begin{equation*}
    U\Delta t = \tau U_0 - RdF_0.
\end{equation*}

Če je $\Delta t$ dovolj majhen, je začeten skok napetosti preprosto

\begin{equation*}
    U_0 = \frac{F_0d}{C}
\end{equation*}

Za čase $t > 0$ enačbi~\ref{eq:diff} odpade drugi člen, njena rešitev je eksponentno upadanje od začetne napetosti, karakterizirano s časom $\tau$

\begin{equation}
    U = U_0 e^{\frac{-t}{\tau}}.
    \label{eq:ansatz}
\end{equation}

Podobno lahko za razbremenitev piezokristala pokažemo, da napetost sprva pade na $-|U_0|$, nato pa nazaj naraste proti začetni vrednosti $0$. Dogovorimo se, da je tam predznak $U_0$ in $F_0$ negativen.

\section{Naloga}

\begin{enumerate}
    \item Določi dielektrično konstanto $\varepsilon$ vzorca iz piezoelektrične keramike.
    \item Izračunaj piezoelektrični koeficient $d$ keramike.
\end{enumerate}

\section{Meritve in račun}

Prvo stehtamo uteži, s katerimi bomo obremenjevali piezokristal (tabela~\ref{tab:mase}). Napaka v izmerjenih masah je

\begin{equation*}
    \Delta m = \SI{1}{g}
\end{equation*}

\begin{table}
    \centering
    \begin{tabular}{c|r}
        $i$ & $m_i\,[\si{g}]$ \\
        \hline
        1 & 196 \\
        2 & 503 \\
        3 & 1007 \\
    \end{tabular}
    \caption{Mase uteži.}
    \label{tab:mase}
\end{table}

S temi utežmi obmremenimo piezokristal in na osciloskopu spremljamo napetost. Sledilnik napetosti nam zagotavlja stalno napetost ozadja, pri obremenitvi pa opazimo začeten skok, ki mu sledi eksponentno padanje (slike~\ref{fig:200}, \ref{fig:500} in \ref{fig:1000} zgoraj). Napetost ozadja $U_b$, ki nas ne zanima, obstranimo tako, da pogledamo del meritev pred skokom napetosti in tja postavimo ničlo. To premik storimo z neko napako $\sigma U_b$, ki jo bomo kaasneje upoštevali. Na eksponenten del zdaj prilagodimo krivuljo v izrazu~\ref{eq:ansatz}. Tako dobimo $U_0$ in $\tau$ (tabela \ref{tab:fall-params}). Za $U_0$ moramo poleg napake $\sigma_{U_0}$, ki je posledica fita, upoštevati tudi napako $\sigma U_b$, ki je navadno precej večja. Skupno napako $U_0$ torej zapišemo

\begin{table}
    \centering
    \begin{tabular}{c|r r |r r |r r | r r}
        $m_i\,[\si{g}]$ & $U_0\,[\si{V}]$ & $\Delta U_0\,[\si{V}]$ & $\tau\,[\si{s}]$ & $\sigma_\tau\,[\si{s}]$ & $C\,[\si{nF}]$ & $\Delta C\,[\si{nF}]$ & $\varepsilon$ & $\Delta\varepsilon$ \\
        \hline
        196 & 0.27 & 0.04 & 10.3 & 2.4 & 2.1 & 0.5 & 1329 & 315 \\
        503 & 0.72 & 0.05 & 8.0 & 0.8 & 1.6 & 0.2 & 1038 & 113 \\
        1007 & 1.28 & 0.05 & 8.8 & 0.5 & 1.8 & 0.1 & 1138 & 76 \\
    \end{tabular}
    \caption{Izračunani $U_0$, $\tau$, $C$ in relativni $\varepsilon$ z napakami pri obremenitvi piezokristala z utežjo $m_i$.}
    \label{tab:fall-params}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{c|r r |r r |r r | r r}
        $m_i\,[\si{g}]$ & $U_0\,[\si{V}]$ & $\Delta U_0\,[\si{V}]$ & $\tau\,[\si{s}]$ & $\sigma_\tau\,[\si{s}]$ & $C\,[\si{nF}]$ & $\Delta C\,[\si{nF}]$ & $\varepsilon$ & $\Delta\varepsilon$ \\
        \hline
        196 & -0.31 & 0.05 & 7.3 & 1.4 & 1.5 & 0.3 & 941 & 186 \\
        503 & -0.89 & 0.08 & 7.8 & 0.6 & 1.6 & 0.1 & 1011 & 93 \\
        1007 & -1.50 & 0.05 & 8.2 & 0.3 & 1.6 & 0.1 & 1067 & 57 \\
    \end{tabular}
    \caption{Izračunani $U_0$, $\tau$, $C$ in relativni $\varepsilon$ z napakami pri razbremenitev piezokristala z utežjo $m_i$.}
    \label{tab:rise-params}
\end{table}

\begin{equation*}
    \Delta U_0 = \sqrt{\sigma_{U_b}^2 + \sigma_{U_b}^2}.
\end{equation*}

Ker poznanamo upor v sledilniku napetosti

\begin{equation*}
    R = (5 \pm 0.1)\,\si{G\ohm},
\end{equation*}

in ker je $\tau = RC$, lahko izračunamo kapacitivnost $C$. Iz te lahko naprej izračunamo še relativno dielektričnost (tabela~\ref{tab:fall-params})

\begin{equation*}
    \varepsilon = \frac{Cb}{\varepsilon_0 S},
\end{equation*}

s tem, da vemo, da je ploščina

\begin{equation*}
    S= (11.3 \pm 0.1)\,\si{cm^2}.
\end{equation*}

Podobno kot za obremenitev lahko zdaj ponovimo še za razbremenitev kristala. Meritve in prilagojene eksponentne krivulje vidimo na slikah~\ref{fig:200}, \ref{fig:500} in \ref{fig:1000} spodaj. Enako kot prej iz $U_0$ in $\tau$ izračunamo $C$ in relativni $\varepsilon$ (tabela~\ref{tab:rise-params}). Z vsemi meritvami izračunamo še povprečje relativnih dielektričnosti

\begin{equation*}
    \overline\varepsilon = 1087 \pm 123.
\end{equation*}

Pri najmanjši uteži (slika~\ref{fig:200} spodaj) smo naredili dva fita -- enega z vsemi meritvami po skoku napetosti, drugega pa z nekoliko odsekanim intervalom meritev. Razlog za to je nepričakovano naraščanje napetosti, ki bi se po modelu morala približevati asimptoti $\Delta U = 0$. Sklepam, da je za to odgovorno obnašanje sledilnika napetosti in da je ta isti mehanizem glaven vir sistematske napake v našem poskusu.


Zdaj izračunamo še piezoelektrični koeficient $d$. Na voljo imamo podatke za odvisnost

\begin{equation*}
    U_0 = \frac{dg}{C} \left( \frac{F_0}{g} \right),
\end{equation*}

pri čemer je $g$ gravitiacijski pospešek. Iz koeficienta $k$, naklona premice na sliki~\ref{fig:U_0-by-m}, lahko piezoelektrični koeficient izračunamo kot

\begin{equation*}
    d = \frac{kC}{g}
\end{equation*}

Ker se različni izračuni za $C$ ne ujemajo, poračunamo povprečno vrednost.

\begin{equation*}
    \overline C = (1.7 \pm 0.2)\,\si{nF},
\end{equation*}

preko česar dobimo piezoelektrični koeficient

\begin{equation*}
    d = (0.24 \pm 0.03)\,\si{nm/V}.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{U_0-by-m.pdf}
    \caption{Odvistnost skoka napetosti $U_0$ od obremenitve $F_0$. Naklon prilagojene premice je $k = \frac{dg}{C}$.}
    \label{fig:U_0-by-m}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{fall-196.pdf}
    \includegraphics{rise-196.pdf}
    \caption{Potek napetosti po obremenitvi in razbremenitvni za $\SI{196}{g}$.}
    \label{fig:200}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{fall-503.pdf}
    \includegraphics{rise-503.pdf}
    \caption{Potek napetosti po obremenitvi in razbremenitvni za $\SI{503}{g}$.}
    \label{fig:500}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{fall-1007.pdf}
    \includegraphics{rise-1007.pdf}
    \caption{Potek napetosti po obremenitvi in razbremenitvni za $\SI{1007}{g}$.}
    \label{fig:1000}
\end{figure}

\end{document}
