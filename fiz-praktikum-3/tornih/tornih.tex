% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\setlength{\skip\footins}{1.5cm}

\title{Torzijsko nihalo}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

O strižni napetosti $F/S$ govorimo takrat, ko leži sila v ravnini ploskve v kateri
prijemlje, v nasprotju s tlačno silo, ki je pravokotna na ploskev. Za strižno napetost velja enačba

\begin{equation*}
    \frac{F}{S} = G\alpha.
\end{equation*}

Torzijski koeficient žice je sorazmernostni faktor med navorom in kotom $\varphi$, zasukom prostega konca žice

\begin{equation*}
    M = D\varphi,
\end{equation*}

Torzijski koeficient je za zasuke, ki niso preveliki, povezan s strižno napetostjo $G$ preko

\begin{equation}
    D = \frac{\pi \rho^4 G}{2\ell},
    \label{eq:D}
\end{equation}

pri čemer je $\rho$ polmer, $\ell$ pa dolžina žice. Če na spodnji konec viseče žice obesimo telo in ga zasukamo, lahko opazujemo torzijsko nihanje. Za majhne zasuke je to nihanje harmonično in velja

\begin{equation}
    t_0 = 2\pi \sqrt{\frac{J}{D}}.
    \label{eq:t_0}
\end{equation}

\section{Naloga}

\begin{enumerate}
    \item Določi torzijski koeficient $D$ žice.
    \item Izračunaj strižni modul $G$ jekla, iz katerega je žica.
    \item Določi vztrajnostni moment in vztrajnostni radij danega telesa (kvadra z valjasto votlino) iz meritve nihajnega časa torzijskega nihala in primerjaj rezultat izračunanim vztrajnostnim momentom.
    \item Določi vztrajnostni moment zobnika.
\end{enumerate}

\section{Meritve in račun}

Prvo izmerimo dimenzije nihala. Žica, ki se pri nihanju torzijsko zvije, je dolga

\begin{equation*}
    \ell = (11.00 \pm 0.02)\,\si{mm},
\end{equation*}

in ima polmer

\begin{equation*}
    \rho = (0.25 \pm 0.01)\,\si{mm}.
\end{equation*}

Če zdaj nihalo zasučemo za majhen kot, izmerimo periodo $t_p$ -- povprečni čas 10-ih nihajev s prazno ploščo. To meritev ponovimo trikrat (tabela~\ref{tab:periods}), nato pa enako storimo če za ploščo z valjem, ploščo s kvadrom in ploščo z zobnikom.

\begin{table}
    \centering
    \begin{tabular}{c|r r r r}
        meritev & $t_p\,[\si{s}]$ & $t_v\,[\si{s}]$ & $t_k\,[\si{s}]$ & $t_z\,[\si{s}]$ \\
        \hline
        1 & 1.909 & 5.366 & 3.715 & 3.031 \\
        2 & 1.909 & 5.378 & 3.722 & 3.016 \\
        3 & 1.912 & 5.375 & 3.691 & 2.997 \\
        \\
        $\overline t\,[\si{s}]$ & 1.910 & 5.373 & 3.709 & 3.015 \\
        $\sigma_t\,[\si{s}]$ & 0.001 & 0.005 & 0.013 & 0.014
    \end{tabular}
    \caption{Povprečja 10-ih period za nihalo samo s ploščo, z valjem, s kvadrom in z zobnikom. Prve tri vrstice vsebujejo meritve, zadnji dve pa povprečja in standarne odmike treh poskusov (vsak je povprečje za 10 nihajev).}
    \label{tab:periods}
\end{table}

Valj z luknjo stehtamo in mu izmerimo notranji premer, zunanji premer, višino

\begin{align*}
    m_v &= (2494 \pm 2)\,\si{g}, \\
    r &= (14.10 \pm 0.02)\,\si{mm}, \\
    R &= (87.30 \pm 0.02)\,\si{mm}, \\
    H &= (49.42 \pm 0.02)\,\si{mm}.
\end{align*}

Iz teh podatkov lahko po znani formuli izračunamo vztrajnostni moment valja

\begin{equation*}
    J_v = (2.438 \pm 0.001)\,\si{gm^2}.
\end{equation*}

Za nihalo z valjem velja po enačbi~(\ref{eq:t_0})

\begin{equation*}
    D = (J_p + J_v) \left( \frac{2\pi}{t_v} \right)^2.
\end{equation*}

Ker je hkrati $J_p = D \left( \frac{t_p}{2\pi} \right)^2$, je

\begin{equation}
    D \left( 1 - \left( \frac{t_p}{t_v} \right)^2 \right) = J_v \left( \frac{2\pi}{t_v} \right)^2.
    \label{eq:D-and-J}
\end{equation}

Izračunamo, da je torzijsko koeficient žice

\begin{equation*}
    D = (3.82 \pm 0.01)\,\si{mNm}.
\end{equation*}

Preko enačbe~(\ref{eq:D}), lahko iz torzijskega koeficienta izračunamo strižni koeficient materiala, iz katerega je narejena žica. Izračunamo strižni koeficient

\begin{equation*}
    G = (7 \pm 1)\,\si{kN/mm^2}.
\end{equation*}

Enačba~(\ref{eq:D-and-J}) ne velja le za $t_v, J_v$, temveč tudi za $t_k, J_k$ in $t_z, J_z$. Velja za poljubno telo, ki ga položimo na ploščo tako, da os vrtenja prebada njegovo težišče. Za kvader z luknjo izračunamo

\begin{equation*}
    J_k = (0.98 \pm 0.01)\,\si{gm^2},
\end{equation*}

za zobnik pa

\begin{equation*}
    J_z = (0.53 \pm 0.01)\,\si{gm^2}.
\end{equation*}

Kvader z luknjo smo prav tako stehtali in izmerili dolžini stranic, višino in premer luknje

\begin{align*}
    m_k &= (1194 \pm 2)\,\si{g} \\
    a &= (60.30 \pm 0.02)\,\si{mm} = b, \\
    h &= (60.10 \pm 0.02)\,\si{mm}, \\
    d &= (40.00 \pm 0.02)\,\si{mm}.
\end{align*}

Tedaj zapišemo izraz za vztrajnostni moment

\begin{equation*}
    J_k = \sigma \left( \frac{S_\square}{12} (a^2 + b^2) - \frac{S_\circ}{2} \left(\frac{d}{2}\right)^2 \right),
\end{equation*}

in izraz za maso kvadra z luknjo

\begin{equation*}
    m_k = \sigma \left( S_\square - S_\circ \right).
\end{equation*}

Če upoštevamo, da je $a = b$ in izraza zdelimo in poenostavimo, dobimo izraz za vztrajnostni moment kvadra z luknjo

\begin{equation*}
    J_k = \frac{m_k}{24} \left( \frac{16a^4 - 3d^4}{4a^2 - \pi d^2} \right).
\end{equation*}

Od tod poračunamo, da je vztrajnostni moment

\begin{equation*}
    J_k = (0.980 \pm 0.002)\,\si{gm^2},
\end{equation*}

kar se ujema z vrednostjo, ki smo jo dobili iz nihajnih časov.

\end{document}
