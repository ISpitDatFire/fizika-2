% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

\title{Vrtavka}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

V približku hitre vrtavke, katere dolga os je poravnana z z-osjo, je nutacijska frekvenca podana z

\begin{equation}
    \nu_\mathrm{nu} = \frac{J_{33}}{J_{11}} \nu_z,
    \label{eq:nu_nu}
\end{equation}

precesijska frekvecna pa z

\begin{equation}
    \nu_\mathrm{pr} = \frac{1}{4\pi^2} \frac{mgh^*}{J_{33} \nu_z}.
    \label{eq:nu_pr}
\end{equation}

\section{Naloga}

\begin{enumerate}
    \item Izmeri precesijsko $\nu_\mathrm{pr}$ in nutacijsko frekvenco $\nu_\mathrm{nu}$ v odvisnosti od frekvence vrtavke $\nu_z$. Izvedi meritev pri vsaj treh frekvencah vrtavke, na primer pri 600, 500 in 400 obratih na minuto (\textit{ang.} rpm).
    \item Gornjo meritev izvedi pri naslednjih nastavitvah vrtavke:
    \begin{enumerate}
        \item vrtavka z utežjo blizu krogle
        \item utež na sredini palice
        \item utež na koncu palice (pusti si prostor za oprijem)
    \end{enumerate}
    Meritve z različnimi nastavitvami vrtavke izvedi pri podobnih frekvencah νz kot prej, da so rezultati lažje primerljivi. Izmerjene vrednosti $\nu_\mathrm{pr}$ in $\nu_\mathrm{nu}$ primerjaj z izračunanimi iz (\ref{eq:nu_pr}) in (\ref{eq:nu_nu}) in naredi tabelo.
\end{enumerate}

\section{Meritve in račun}

Pri različnih frekvencah vrtavke $\nu_z$ pomerimo čas 10 precesij vrtavke. Iz tega izračunamo precesijsko frekvenco. Pri višjih frekvencah, kjer je vrtavka bolj stabilna, jo izmaknemo, da začne nutirati in to gibanje posnamemo. Nutacijske frekvence zračunamo s štetjem sličic v teh $\SI{30}{fps}$ posnetkih vrtavke. Vse zapišemo v tabele~\ref{tab:top},~\ref{tab:mid},~\ref{tab:top}.

Lotimo se izračuna frekvenc. Prvo iz dimenzij krogle

\begin{equation*}
    m_s = \SI{515}{g} \qquad 2r_s = \SI{50.8}{mm},
\end{equation*}

in dimenzij treh valjev

\begin{align*}
    m_r &= \SI{15}{g} \qquad 2r_r = \SI{51}{g} \qquad h_r = \SI{1.1}{mm} \\
    m_b &= \SI{27}{g} \qquad 2r_b = \SI{6.5}{g} \qquad h_b = \SI{100.5}{mm} \\
    m_w &= \SI{18}{g} \qquad 2r_1 = \SI{20}{g} \qquad h_w = \SI{25.2}{mm},
\end{align*}

izračunamo višino težišča vrtavke glede na središče krogle in vztrajnostni moment vrtavke glede na to težišče. To izračunamo pri različnih vrednostih parametra $\ell$ (slika~\ref{fig:skica}) oz. parametra $\ell' = \ell - h_w/2$, ki smo ga dejansko merili. Vse zapišemo v tabelo~\ref{tab:J}

\begin{table}
    \centering
    \begin{tabular}{c|r|r r r}
        $\ell'\,[\si{mm}]$ & $h^*\,[\si{mm}]$ & $J_{11}\,[\si{kg\,cm^2}]$ & $J_{11}\,[\si{kg\,cm^2}]$ & $J_{33}\,[\si{kg\,cm^2}]$ \\
        \hline
        0 & 5.18 & 3.25 & 3.25 & 3.25 \\
        $36 \pm 1$ & 6.31 & 3.81 & 3.81 & 3.81 \\
        $61 \pm 1$ & 7.09 & 4.46 & 4.46 & 4.46
    \end{tabular}
    \caption{Za različne $\ell' = \ell - h_w/2$ izračunane ročke težišča in diagonalni elementi vztrajnostnega tenzorja (izvendiagonalni elementi so ničelni, saj je z-os poravnana s simetrijsko osjo vrtavke).}
    \label{tab:J}
\end{table}

Iz dobljene ročk težišča in vztrajnostnih momentov lahko po~(\ref{eq:nu_nu}) in~(\ref{eq:nu_nu}) izračunamo pričakovane frekvence nutacija in precesije. Vse zapišemo v tabele~\ref{tab:top},~\ref{tab:mid},~\ref{tab:top}.

Izmerjene in izračunane frekvence se kljub zelo konzervativnim napakam ne ujemajo. Sklepam, da so izvori napake slednji.

Da bi prihranil čas z šibkim kompresorjem sem meritve izvajal pri nižjih frekvencah $\nu_z$. V enačbah~(\ref{eq:nu_nu}),~(\ref{eq:nu_nu}) predpostavljamo, da je vrtavka hitra. To pomeni, da je razmerje potencialne proti kinetični energiji $\rho$ veliko manjše od 1. A razmerje $\rho$ je pri naših meritvah.

Dodatno so lahko k napaki prispevali preveliki začetni izmiki pri nutaciji.

Poraja se tudi možnost, da je bila dejanska frekvenca $\nu_z$ dvakrat večja od te, ki sem jo zapisal v beležko. Tega namreč na stroboskopu ne bi opazil in delno bi razložilo izračunane, sistematsko prevelike precesijske in sistematsko premajhne nutacijske frekvence.

\begin{equation*}
    \rho = \frac{mgh^*}{2\pi^2 J_{33}\nu_z^2}
\end{equation*}

\begin{figure}
    \centering
    \import{}{dimensions.pdf_tex}
    \caption{Skica simetrične vrtavke.}
    \label{fig:skica}
\end{figure}


\begin{table}
    \centering
    \begin{tabular}{c}
        izračunano \\
        \begin{tabular}{r|r|r|r}
            \hline
            $\nu_z\,[\si{rpm}]$ & $\rho$& $\nu_\mathrm{pr}\,[\si{Hz}]$ & $\nu_\mathrm{nu}\,[\si{Hz}]$ \\
            \hline
            $226 \pm 50$ & 1.5 & $1.42 \pm 0.31$ & $1.6 \pm 0.4$ \\
            $400 \pm 50$ & 0.5 & $0.80 \pm 0.10$ & $2.8 \pm 0.4$ \\
            $500 \pm 50$ & 0.3 & $0.64 \pm 0.06$ & $3.6 \pm 0.4$
        \end{tabular} \\
        \\
        izmerjeno \\
        \begin{tabular}{r r|r r|r}
            \hline
            $\nu_z\,[\si{rpm}]$ & $\nu_z\,[\si{Hz}]$ & $10t_\mathrm{pr}\,[\si{s}]$ & $\nu_\mathrm{pr}\,[\si{Hz}]$ & $\nu_\mathrm{nu}\,[\si{Hz}]$ \\
            \hline
            $226 \pm 50$ & $3.8 \pm 0.8$ & $19 \pm 1$ & $0.54 \pm 0.03$ &               \\
            $400 \pm 50$ & $4.7 \pm 0.8$ & $23 \pm 1$ & $0.43 \pm 0.02$ & $4.5 \pm 0.2$ \\
            $500 \pm 50$ & $8.3 \pm 0.8$ & $29 \pm 1$ & $0.35 \pm 0.01$ & $6.5 \pm 0.8$
        \end{tabular}
    \end{tabular}
    \caption{Izračunane pričakovane in izmerjene frekvence za $\ell' = 0$. Pri izmerjenih je naveden tudi parameter $\rho$ -- razmerje med potencialno in kinetično energijo vrtavke.}
    \label{tab:bottom}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{c}
        izračunano \\
        \begin{tabular}{r|r|r|r}
            \hline
            $\nu_z\,[\si{rpm}]$ & $\rho$& $\nu_\mathrm{pr}\,[\si{Hz}]$ & $\nu_\mathrm{nu}\,[\si{Hz}]$ \\
            \hline
            $300 \pm 50$ & 1.0 & $1.3 \pm 0.2$ & $1.8 \pm 0.3$ \\
            $400 \pm 50$ & 0.6 & $1.0 \pm 0.1$ & $2.4 \pm 0.3$ \\
            $500 \pm 50$ & 0.4 & $0.78 \pm 0.08$ & $3.0 \pm 0.3$ \\
        \end{tabular} \\
        \\
        izmerjeno \\
        \begin{tabular}{r r|r r|r}
            \hline
            $\nu_z\,[\si{rpm}]$ & $\nu_z\,[\si{Hz}]$ & $10t_\mathrm{pr}\,[\si{s}]$ & $\nu_\mathrm{pr}\,[\si{Hz}]$ & $\nu_\mathrm{nu}\,[\si{Hz}]$ \\
            \hline
            $300 \pm 2$ & $5.00 \pm 0.03$ & $12 \pm 1$ & $0.87 \pm 0.07$   &                 \\
            $400 \pm 2$ & $6.67 \pm 0.03$ & $17 \pm 1$ & $0.59 \pm 0.03$ & $3.3 \pm 0.2$ \\
            $500 \pm 2$ & $8.33 \pm 0.03$ & $23 \pm 1$ & $0.44 \pm 0.02$ & $4.5 \pm 0.1$
        \end{tabular}
    \end{tabular}
    \caption{Izračunane pričakovane in izmerjene frekvence za $\ell' = (36 \pm 1)\,\si{mm}$. Pri izmerjenih je naveden tudi parameter $\rho$ -- razmerje med potencialno in kinetično energijo vrtavke.}
    \label{tab:mid}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{c}
        izračunano \\
        \begin{tabular}{r|r|r|r}
            \hline
            $\nu_z\,[\si{rpm}]$ & $\rho$& $\nu_\mathrm{pr}\,[\si{Hz}]$ & $\nu_\mathrm{nu}\,[\si{Hz}]$ \\
            \hline
            $300 \pm 50$ & 1.2 & $1.46 \pm 0.24$ & $1.6 \pm 0.3$ \\
            $400 \pm 50$ & 0.7 & $1.09 \pm 0.14$ & $2.1 \pm 0.3$ \\
            $500 \pm 50$ & 0.4 & $0.88 \pm 0.09$ & $2.6 \pm 0.3$ \\
        \end{tabular} \\
        \\
        izmerjeno \\
        \begin{tabular}{r r|r r|r}
            \hline
            $\nu_z\,[\si{rpm}]$ & $\nu_z\,[\si{Hz}]$ & $10t_\mathrm{pr}\,[\si{s}]$ & $\nu_\mathrm{pr}\,[\si{Hz}]$ & $\nu_\mathrm{nu}\,[\si{Hz}]$ \\
            \hline
            $300 \pm 2$ & $5.00 \pm 0.03$ & $6.47 \pm 1$  & $1.6 \pm 0.2$   &                 \\
            $400 \pm 2$ & $6.67 \pm 0.03$ & $15.42 \pm 1$ & $0.65 \pm 0.04$   & $2.5 \pm 0.1$ \\
            $500 \pm 2$ & $8.33 \pm 0.03$ & $19.28 \pm 1$ & $0.52 \pm 0.03$ & $3.6 \pm 0.2$
        \end{tabular}
    \end{tabular}
    \caption{Izračunane pričakovane in izmerjene frekvence za $\ell' = (61 \pm 1)\,\si{mm}$. Pri izmerjenih je naveden tudi parameter $\rho$ -- razmerje med potencialno in kinetično energijo vrtavke.}
    \label{tab:top}
\end{table}

\end{document}
