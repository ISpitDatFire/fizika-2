% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{subfig}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage[svgnames]{xcolor}

% Pgfplots
\usepackage{amssymb}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}

\title{Uklon svetlobe}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

\begin{figure}[ht]
    \centering
    \begin{tikzpicture}[scale=0.5]
        \draw[fill=black] (0,-1) rectangle (0.4,-4);
        \draw[fill=black] (0,1) rectangle (0.4,4);
        \draw[dashed,thick] (0.2,-1)  -- (0.2,1);

        \draw[-latex,thick] (0.2,0.5) -- (2,0.5) node[label={$\hat{\mathbf{n}}$}]{};
        \draw[-latex,thick] (-4, 0) -- node[above]{$\mathbf r_i$} (0.2,0.5) ;
        \draw[-latex,thick] (0.2,0.5) -- node[above]{$\mathbf r_z$} (10,-2);

        \node[left] at (0.2,-0.5) {$\mathcal S$};
        \node[label=below:{izvor}] at (-4,0) {\textbullet};

        \draw[very thick] (10,-3.2) -- (10,3.2);
        \node[left] at (10, 0) {zaslon};
    \end{tikzpicture}
    \caption{Skica izvora, reže $\mathcal S$ in zaslona.}
    \label{fig:diffraction-diagram}
\end{figure}

Ključnen izmed pojav v optiki, je uklon. Ima bogato fenomenologijo in mero razumevanja lahko dosežemo preko Huygensovega principa, ki pravi, da lahko vsako točko na valovni fronti obravnavamo kot nov točkast izvor. A ta razlaga ima nekaj pomankljivosti -- bolj dodelana teoretična razlaga uklona je nekoliko bolj subtilna in se je razvila šele proti koncu 19. stoletja. Če stvari označimo kot na skici~\ref{fig:diffraction-diagram}, velja za amplitudo na zaslonu t. i. Fresnel-Kirchhoffova enačba

\begin{equation}
    u_z  = \frac{Ak}{4\pi i} \int_\mathcal{S} \left[ \frac{\mathbf r_i \cdot \hat{\mathbf{n}}}{r_i} - \frac{\mathbf r_z \cdot \hat{\mathbf{n}}}{r_z} \right] \frac{e^{ik(r_i + r_z)}}{r_i r_z} \,\diff S
    \label{eq:F-K}
\end{equation}

Tega integrala ne moremo rešiti brez določenih poenostavitev. Če predpostavimo, da sta vektorja $\mathbf r_i$, $\mathbf r_z$ približno vzporedna z $\hat{\mathbf{n}}$ in da je zaslon daleč stran od uklonske mreže. Izračunamo lahko, da je daleč od uklonskih rež v t. i. Fraunhoferjevem približku jakost svetlobe $j \propto |u_z|^2$ na zaslonu oblike

\begin{equation}
    j(\phi) = \frac{j_0}{n^2} \left( \frac{\sin(k\phi d/2)}{k\phi d/2} \right)^2  \left( \frac{\sin(nk\phi\ell/2)}{\sin(k\phi\ell/2)} \right)^2.
    \label{eq:Fraun}
\end{equation}

Pri tem je $n$ število uklonskih rež, $d$ njihova širina in $\ell$ razdalja med eno in drugo režo.
Za uklon na okrogli reži lahko izračunamo jakost svetlobe v sredini uklonskega vzorca, ki je od radija reže $r$ odvisna kot

\begin{equation}
    j(r) = j_0 \sin^2 \left( \frac{kr^2}{4R} \right),
    \label{eq:Fresnel}
\end{equation}

pri čemer smo $R$ definirali tako, da je $R^{-1} = z_f^{-1} + z_z^{-1}$. Pri tem je $z_z$ razdalja od uklonskih rež do zaslona, $z_f$ pa razdalja od rež do gorišča leče, s katero iz laserja ustvarimo približno točkovni izvor (oboje merjeno vzdolž optične osi). Dobimo torej periodične ekstreme sredinske jakosti, v katerih velja

\begin{equation}
    n = \frac{r^2}{\lambda R},
    \label{eq:ekstreme}
\end{equation}

pri čemer je $n$ liho število za maksimume jakosti ter sod za minimume.

\section{Naloga}

\begin{enumerate}
    \item Izmeri uklonsko sliko svetlobe za zasloni z režami. Uporabi zaslone z 1, 2, 3 in 5 režami. Določi relativne intenzitete uklonskih slik. Določi širino rež $d$ in razdalje med njimi $\ell$.
    \item Opazuj uklon na okrogli odprtini. Določi premer odprtine $2r$.
\end{enumerate}

\section{Meritve in račun}

\subsection{Fraunhoferjev uklon}

Laserski žarek namerimo v sredinsko lego pomičnega senzorja. V žarek postavimo eno uklonsko mrežo. Razdalja od reže do senzorja je

\begin{equation*}
    L = (200 \pm 2)\,\si{cm}.
\end{equation*}

Senzor povežemo na računalnik, tako da stalno vzorči jakost $j(x)$, pri položaju senzorja $x$, ki ga merimo preko spremenljive dolžine uporne žice, ki jo s premikanjem senzorja navijamo na vijak. Enako meritev opravimo tudi za 2, 3 in 5 rež. Dobljene odvisnosti vidimo na~(\ref{fig:n-slits}). Skozi meritve za 1 režo prilagodimo modelsko funkcijo~(\ref{eq:Fraun}), ki se poenostavi v drugem faktorju in ima le en parameter -- debelino uklonske reže $d$. Izračunamo, da je debelina reže

\begin{figure}
    \centering
    \includegraphics{n-slits.pdf}
    \caption{Izmerjene jakosti Fraunhoferjevega uklona na $n$ režah ter prilagojene modelske krivulje.}
    \label{fig:n-slits}
\end{figure}

\begin{equation*}
    d = (21.99 \pm 0.02)\,\si{\mu m}.
\end{equation*}

Z dobljeno vrednostjo $d$ ima modelska funkcija~(\ref{eq:Fraun}) tudi pri višjih $n$ le en parameter -- razdalno med režami $\ell$. Pri postavitvi z 2, 3 in 5 režami izračunamo, da je razdalja med režami

\begin{align*}
    \ell_2 &= (99.7 \pm 0.1)\,\si{\mu m}, \\
    \ell_3 &= (100.1 \pm 0.3)\,\si{\mu m}, \\
    \ell_5 &= (99.7 \pm 0.5)\,\si{\mu m}.
\end{align*}

\subsection{Fresnelov uklon}

Zdaj bomo uklonski vzorec opazovali s prostim očesom. Za laser postavimo zbiralno lečo, v stojalo pa okroglo režo. Začetna razdalja med lečo in režo je

\begin{equation*}
    d_0 = (11.2 \pm 0.2)\,\si{cm}.
\end{equation*}

Med lečo in zaslonom, na katerem opazujemo, pa

\begin{equation*}
    z_{z0} = (144.5 \pm 2)\,\si{cm}.
\end{equation*}

Zdaj stojalo z režo premikamo stran od laserja in leče, tako da je razdalja od gorišča leče do reže $z_f = d_0 - f + x$, razdalja od reže do zaslona pa $z_z = z_{0z} - x$. Premikamo vijak in gledamo, kje je sredina uklonskega vzorca najsvetlejša in najtemnejša. Radij uklonske reže $r$ konstanten, s premikanjem stojala pa med zaporednimi ekstremi spreminjamo parameter $R^{-1} = z_f^{-1} + z_z^{-1}$. V teh ekstremih iz izraza~(\ref{eq:Fresnel}) pričakujemo, da velja

\begin{equation*}
    \frac{1}{\lambda R} = \frac{1}{r^2} n,
\end{equation*}

torej dobimo iz zaporednih ekstremov premico~(slika~\ref{fig:fresnel-zones}), katere naklon je preprosto $1/r^2$, torej je radij uklonske reže

\begin{equation*}
    r = (0.72 \pm 0.04)\,\si{mm}
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{fresnel-zones.pdf}
    \caption{Vrednosti $\frac{1}{\lambda R_n}$ v zaporednih ($\Delta n = 1$) ekstremih sredinske jakosti uklonskega vzorca. Svetle cone so minimumi, temne pa maksimumi jakosti~(\ref{eq:Fresnel}). Tako vemo, da je $n_0$ liho število. Naklon prilagojene premice je $1/r^2$.}
    \label{fig:fresnel-zones}
\end{figure}

\end{document}
