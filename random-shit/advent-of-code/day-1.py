import requests


# Get the input file
day = 1
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
def n_increases(list):
    return sum([list[i] > list[i-1] for i in range(1, len(list))])

h = [int(line) for line in input.splitlines()]
h_window = [u + v + w for u, v, w in zip(h, h[1:], h[2:])]
print(n_increases(h))
print(n_increases(h_window))
