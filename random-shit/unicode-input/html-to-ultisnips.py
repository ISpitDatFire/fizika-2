from lxml import html

with (open(r'./julia-unicode-input.html', 'r') as infile,
      open(r'./all.snippets', 'w') as outfile):
    tree = html.parse(infile)
    table = tree.find('.//table')
    for tr in table[1:]:
        char, code, name = tr[1].text, tr[2].text, tr[3].text
        # Use only the first trigger.
        code = code.split(',')[0]
        # Remove colons and replace underscores with dashes.
        code = code.replace(':', '')
        code = code.replace('_', '-')
        # Rewrite with UltisSnips syntax.
        outfile.write(
            f'snippet {code} "{name}" i\n{char}\nendsnippet\n\n')
